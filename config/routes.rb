Rails.application.routes.draw do
  
 resources :users 
 root 'static_pages#home' 


	get '/about' => "static_pages#about"
	get '/help' => "static_pages#help", as: 'helf'  
	get '/contact' => "static_pages#contact"
	get '/signup' => "users#new"
post '/signup', to: 'users#create'

  get 'static_pages/home'
  get 'static_pages/help'
 
end
